#version 130
uniform vec3 uLight, uLight2, uColor; 
 
in vec3 vNormal; // normal to surface
in vec3 vPosition; // position of point on surface
 
out vec4 fragColor;
 
void main() {
  vec3 toLight = uLight - vec3(vPosition);
  vec3 toLight2 = uLight2 - vec3(vPosition);
  vec3 toV = -normalize(vec3(vPosition));
  toLight = normalize(toLight);
  toLight2 = normalize(toLight2);
  vec3 h1 = normalize(toV + toLight);
  vec3 h2 = normalize(toV + toLight2);
  vec3 normal = normalize(vNormal);


  float specular;
  float diffuse;
  if(gl_FrontFacing == false){
    specular = pow(max(0.0, dot(h1, -normal)), 64.0) + pow(max(0.0, dot(h2, -normal)), 64.0);
    diffuse = max(0.1, dot(-normal, toLight));
    diffuse += max(0.1, dot(-normal, toLight2));
  }
  else{
    specular = pow(max(0.0, dot(h1, normal)), 64.0) + pow(max(0.0, dot(h2, normal)), 64.0);
    diffuse = max(0.1, dot(normal, toLight));
    diffuse += max(0.1, dot(normal, toLight2));
  }
  vec3 intensity =  vec3(0.1,0.1,0.1) + uColor * diffuse + vec3(0.6,0.6,0.6) * specular;

  fragColor = vec4(intensity.x, intensity.y, intensity.z, 1.0);
}
