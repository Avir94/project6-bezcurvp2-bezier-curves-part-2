#include <vector>
#include <string>
#include <memory>
#include <stdexcept>
#include <math.h>
#if __GNUG__
#   include <tr1/memory>
#endif

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

#include "cvec.h"
#include "matrix4.h"
#include "geometrymaker.h"
#include "ppm.h"
#include "glsupport.h"

using namespace std; // for string, vector, iostream, and other standard C++ stuff
using namespace tr1; // for shared_ptr
		   
class Bezier{
  
  Matrix4 p0;
  Matrix4 p1;
  Matrix4 p2;
  Matrix4 p3;

public:
  Bezier(Matrix4 p0, Matrix4 p1, Matrix4 p2, Matrix4 p3){
    setP(p0, p1, p2, p3);
  }

  void setp0(Matrix4 p){
  p0 = p;
  }
 
  void setp1(Matrix4 p){
    p1 = p;
  }
  
  void setp2(Matrix4 p){
    p2 = p;
  }

  void setp3(Matrix4 p){
    p3 = p;
  }

  void setP(Matrix4 s0, Matrix4 s1, Matrix4 s2, Matrix4 s3){
    setp0(s0);
    setp1(s1);
    setp2(s2);
    setp3(s3);    
  }
  
  Matrix4 getBez(double t){
    return Matrix4::makeTranslation(Cvec3((pow((1-t),3) * p0[ 3]) + (3 * t * pow((1-t),2) * p1[ 3]) + (3 * pow(t,2) * (1-t) * p2[ 3]) + (pow(t,3) * p3[ 3]),
					  (pow((1-t),3) * p0[ 7]) + (3 * t * pow((1-t),2) * p1[ 7]) + (3 * pow(t,2) * (1-t) * p2[ 7]) + (pow(t,3) * p3[ 7]),
					  (pow((1-t),3) * p0[11]) + (3 * t * pow((1-t),2) * p1[11]) + (3 * pow(t,2) * (1-t) * p2[11]) + (pow(t,3) * p3[11])
					  )
				    );
  }
  
};
  
