////////////////////////////////////////////////////////////////////////
//
//   Model a system with dynamic equilibrium.  Uses Phong lighting,
//   time-based animation, and homemade geometry objects.
//
//   This skeleton code demos the use of a time-based animation clock.
//
//   Westmont College
//   CS150 : 3D Computer Graphics
//   Professor David Hunter
//
//   Some code is from  _Foundations of 3D Computer Graphics_
//   by Steven Gortler.  See AUTHORS file for more details.
//
////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <memory>
#include <stdexcept>
#include <math.h>
#if __GNUG__
#   include <tr1/memory>
#endif

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

#include "cvec.h"
#include "matrix4.h"
#include "geometrymaker.h"
#include "ppm.h"
#include "glsupport.h"
#include "Bezier.cpp"

using namespace std; // for string, vector, iostream, and other standard C++ stuff
using namespace tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

// --------- IMPORTANT --------------------------------------------------------
// Before you start working on this assignment, set the following variable
// properly to indicate whether you want to use OpenGL 2.x with GLSL 1.0 or
// OpenGL 3.x+ with GLSL 1.3.
//
// Set g_Gl2Compatible = true to use GLSL 1.0 and g_Gl2Compatible = false to
// use GLSL 1.3. Make sure that your machine supports the version of GLSL you
// are using. In particular, on Mac OS X currently there is no way of using
// OpenGL 3.x with GLSL 1.3 when GLUT is used.
//
// If g_Gl2Compatible=true, shaders with -gl2 suffix will be loaded.
// If g_Gl2Compatible=false, shaders with -gl3 suffix will be loaded.
// To complete the assignment you only need to edit the shader files that get
// loaded
// ----------------------------------------------------------------------------
static const bool g_Gl2Compatible = false;

static float g_frustMinFov = 60.0;        // Show at least 60 degree field of view
static float g_frustFovY = g_frustMinFov; // FOV in y direction (updated by updateFrustFovY)

static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane

static int g_windowWidth = 512;
static int g_windowHeight = 512;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;
static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event
static int g_activeShader = 0;
static int g_objToManip = 2;  // object to manipulate 

  // Animation globals for time-based animation
static const float g_animStart = 0;
static const float g_animMax = 5; 
static float g_animClock = g_animStart; // clock parameter runs from g_animStart to g_animMax then repeats
static float g_animSpeed = 0.5;         // clock units per second
static int g_elapsedTime = 0;           // keeps track of how long it takes between frames
static float g_animIncrement = g_animSpeed/60.0; // updated by idle() based on GPU speed
static int view = 0; // which object to look through: 0=eye, 1=animated object
static bool viewPath = false; // to see the bezier curve path or not

struct ShaderState {
  GlProgram program;

  // Handles to uniform variables
  GLint h_uLight, h_uLight2; // two lights
  GLint h_uProjMatrix;
  GLint h_uModelViewMatrix;
  GLint h_uNormalMatrix;
  GLint h_uColor;

  // Handles to vertex attributes
  GLint h_aPosition;
  GLint h_aNormal;

  ShaderState(const char* vsfn, const char* fsfn) {
    readAndCompileShader(program, vsfn, fsfn);

    const GLuint h = program; // short hand

    // Retrieve handles to uniform variables
    h_uLight = safe_glGetUniformLocation(h, "uLight");
    h_uLight2 = safe_glGetUniformLocation(h, "uLight2");
    h_uProjMatrix = safe_glGetUniformLocation(h, "uProjMatrix");
    h_uModelViewMatrix = safe_glGetUniformLocation(h, "uModelViewMatrix");
    h_uNormalMatrix = safe_glGetUniformLocation(h, "uNormalMatrix");
    h_uColor = safe_glGetUniformLocation(h, "uColor");

    // Retrieve handles to vertex attributes
    h_aPosition = safe_glGetAttribLocation(h, "aPosition");
    h_aNormal = safe_glGetAttribLocation(h, "aNormal");

    if (!g_Gl2Compatible)
      glBindFragDataLocation(h, 0, "fragColor");
    checkGlErrors();
  }
};

static const int g_numShaders = 2;
static const char * const g_shaderFiles[g_numShaders][2] = {
  {"./shaders/basic-gl3.vshader", "./shaders/solid-gl3.fshader"},
  {"./shaders/basic-gl3.vshader", "./shaders/phong-gl3.fshader"}
};
static const char * const g_shaderFilesGl2[g_numShaders][2] = {
  {"./shaders/basic-gl2.vshader", "./shaders/solid-gl2.fshader"},
  {"./shaders/basic-gl2.vshader", "./shaders/phong-gl2.fshader"}
};
static vector<shared_ptr<ShaderState> > g_shaderStates; // our global shader states

// --------- Geometry

// Macro used to obtain relative offset of a field within a struct
#define FIELD_OFFSET(StructType, field) &(((StructType *)0)->field)

// A vertex with floating point Position, Normal, and one set of teXture coordinates;
struct VertexPNX {
  Cvec3f p, n; // position and normal vectors
  Cvec2f x; // texture coordinates

  VertexPNX() {}

  VertexPNX(float x, float y, float z,
            float nx, float ny, float nz,
            float u, float v)
    : p(x,y,z), n(nx, ny, nz), x(u, v) 
  {}

  VertexPNX(const Cvec3f& pos, const Cvec3f& normal, const Cvec2f& texCoords)
    :  p(pos), n(normal), x(texCoords) {}

  VertexPNX(const Cvec3& pos, const Cvec3& normal, const Cvec2& texCoords)
    : p(pos[0], pos[1], pos[2]), n(normal[0], normal[1], normal[2]), x(texCoords[0], texCoords[1]) {}

  // Define copy constructor and assignment operator from GenericVertex so we can
  // use make* function templates from geometrymaker.h
  VertexPNX(const GenericVertex& v) {
    *this = v;
  }

  VertexPNX& operator = (const GenericVertex& v) {
    p = v.pos;
    n = v.normal;
    x = v.tex;
    return *this;
  }
};

struct Geometry {
  GlBufferObject vbo, ibo;
  int vboLen, iboLen;

  Geometry(VertexPNX *vtx, unsigned short *idx, int vboLen, int iboLen) {
    this->vboLen = vboLen;
    this->iboLen = iboLen;

    // Now create the VBO and IBO
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPNX) * vboLen, vtx, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * iboLen, idx, GL_STATIC_DRAW);
  }

  void draw(const ShaderState& curSS) {
    // Enable the attributes used by our shader
    safe_glEnableVertexAttribArray(curSS.h_aPosition);
    safe_glEnableVertexAttribArray(curSS.h_aNormal);

    // bind vertex buffer object
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    safe_glVertexAttribPointer(curSS.h_aPosition, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, p));
    safe_glVertexAttribPointer(curSS.h_aNormal, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, n));

    // bind index buffer object
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

    // draw!
    glDrawElements(GL_TRIANGLES, iboLen, GL_UNSIGNED_SHORT, 0);

    // Disable the attributes used by our shader
    safe_glDisableVertexAttribArray(curSS.h_aPosition);
    safe_glDisableVertexAttribArray(curSS.h_aNormal);
  }
};

// Vertex buffer and index buffer associated with the different geometries
static shared_ptr<Geometry> g_cube, g_sphere, g_octahedron, g_tube, g_cone; 

// --------- Scene

static const Cvec3 g_light1(2.0, 3.0, 14.0), g_light2(-2, -3.0, -5.0);  // define two light positions in world space
static Matrix4 g_eyeRbt = Matrix4::makeTranslation(Cvec3(0.0, 3.25, 10.0));
static const int g_numObjects = 18;
static Matrix4 g_objectRbt[g_numObjects] = {Matrix4::makeTranslation(Cvec3(0,4,0)),    // animated object 
					    Matrix4::makeTranslation(Cvec3(0,0,0)),    // cube scenery
					    // curve 1 (2-5)
					    Matrix4::makeTranslation(Cvec3(-8,4,1)),   // control point 1/20 
					    Matrix4::makeTranslation(Cvec3(-1,2,7)),   // control point 2
					    Matrix4::makeTranslation(Cvec3(-2,1,0)),    // control point 3
					    Matrix4::makeTranslation(Cvec3(0,3,-3)),   // control point 4/5
					    // curve 2 (6-9)
					    Matrix4::makeTranslation(Cvec3(2,5,-6)),   // control point 6
					    Matrix4::makeTranslation(Cvec3(3,-1,-5)),    // control point 7 
					    Matrix4::makeTranslation(Cvec3(7,-2,-7)),   // control point 8/9
					    // curve 3 (10-13)
					    Matrix4::makeTranslation(Cvec3(11,-3,-9)),  // control point 10 
					    Matrix4::makeTranslation(Cvec3(10,-1,-5)),   // control point 11
					    Matrix4::makeTranslation(Cvec3(7,-2,-3)),  // control point 12/13
					    // curve 4 (14-17)
					    Matrix4::makeTranslation(Cvec3(4,-3,-1)),  // control point 14
					    Matrix4::makeTranslation(Cvec3(3,-3,-2)),  // control point 15
					    Matrix4::makeTranslation(Cvec3(0,-1,-3)),   // control point 16/17
					    // curve 5 (18-20)
					    Matrix4::makeTranslation(Cvec3(-3,1,-4)),   // control point 18
					    Matrix4::makeTranslation(Cvec3(-15,6,-5)),  // control point 19
					    Matrix4::makeTranslation(Cvec3(-8,4,1)),   // control point 1/20 
}; // each object gets its own RBT  

///////////////// END OF G L O B A L S //////////////////////////////////////////////////

static void initObjects() {
  // each kind of geometry needs to be initialized here
  int ibLen, vbLen;
  getCubeVbIbLen(vbLen, ibLen);

  // Temporary storage for vertex and index buffers
  vector<VertexPNX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeCube(2, vtx.begin(), idx.begin());
  g_cube.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));

  getSphereVbIbLen(50, 50, vbLen, ibLen);
  vtx.resize(vbLen);
  idx.resize(ibLen);
  makeSphere(1.0, 50, 50, vtx.begin(), idx.begin());
  g_sphere.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));

  getOctahedronVbIbLen(vbLen, ibLen);
  vtx.resize(vbLen);
  idx.resize(ibLen);
  makeOctahedron(1.25, vtx.begin(), idx.begin());
  g_octahedron.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));  

  getTubeVbIbLen(50, vbLen, ibLen);
  vtx.resize(vbLen);
  idx.resize(ibLen);
  makeTube(.25, 2, 50, vtx.begin(), idx.begin());
  g_tube.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen)); 

  getConeVbIbLen(50, vbLen, ibLen);
  vtx.resize(vbLen);
  idx.resize(ibLen);
  makeCone(.5, 1, 50, vtx.begin(), idx.begin());
  g_cone.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));
}

// takes a projection matrix and send to the the shaders
static void sendProjectionMatrix(const ShaderState& SS, const Matrix4& projMatrix) {
  GLfloat glmatrix[16];
  projMatrix.writeToColumnMajorMatrix(glmatrix); // send projection matrix
  safe_glUniformMatrix4fv(SS.h_uProjMatrix, glmatrix);
}

// takes MVM and its normal matrix to the shaders
static void sendModelViewNormalMatrix(const ShaderState& SS, const Matrix4& MVM, const Matrix4& NMVM) {
  GLfloat glmatrix[16];
  MVM.writeToColumnMajorMatrix(glmatrix); // send MVM
  safe_glUniformMatrix4fv(SS.h_uModelViewMatrix, glmatrix);

  NMVM.writeToColumnMajorMatrix(glmatrix); // send NMVM
  safe_glUniformMatrix4fv(SS.h_uNormalMatrix, glmatrix);
}

// update g_frustFovY from g_frustMinFov, g_windowWidth, and g_windowHeight
static void updateFrustFovY() {
  if (g_windowWidth >= g_windowHeight)
    g_frustFovY = g_frustMinFov;
  else {
    const double RAD_PER_DEG = 0.5 * CS150_PI/180;
    g_frustFovY = atan2(sin(g_frustMinFov * RAD_PER_DEG) * g_windowHeight / g_windowWidth, cos(g_frustMinFov * RAD_PER_DEG)) / RAD_PER_DEG;
  }
}

static Matrix4 makeProjectionMatrix() {
  return Matrix4::makeProjection(
           g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
           g_frustNear, g_frustFar);
}

static Matrix4 bezier(double t, Matrix4 p0, Matrix4 p1, Matrix4 p2, Matrix4 p3){
  return Matrix4::makeTranslation(Cvec3((pow((1-t),3) * p0[ 3]) + (3 * t * pow((1-t),2) * p1[ 3]) + (3 * pow(t,2) * (1-t) * p2[ 3]) + (pow(t,3) * p3[ 3]),
					(pow((1-t),3) * p0[ 7]) + (3 * t * pow((1-t),2) * p1[ 7]) + (3 * pow(t,2) * (1-t) * p2[ 7]) + (pow(t,3) * p3[ 7]),
					(pow((1-t),3) * p0[11]) + (3 * t * pow((1-t),2) * p1[11]) + (3 * pow(t,2) * (1-t) * p2[11]) + (pow(t,3) * p3[11])
					)
				  );
}


static void drawScene() {
  const Matrix4 projmat = makeProjectionMatrix(); // build projection matrix

  // g_animIncrement is a small amount that is scaled to the framerate; the
  // following matrix will rotate through a small angle so that a total of 
  // 360 degrees is covered for every cycle of the clock parameter g_animClock
  // from 0 to 1.
  const Matrix4 rotatorY = Matrix4::makeYRotation(g_animIncrement*360); // rotate 360 per parameter cycle 0..1
  const Matrix4 moveX = Matrix4::makeTranslation(Cvec3(.001,0,0));
  const ShaderState& curSS = *g_shaderStates[1]; // will always use phong shader
  const ShaderState& solidSS = *g_shaderStates[0]; // will always use solid shader
  
  Matrix4 path;

  Bezier bc1(g_objectRbt[ 2], g_objectRbt[ 3], g_objectRbt[4], g_objectRbt[5]);
  Bezier bc2(g_objectRbt[ 5], g_objectRbt[ 6], g_objectRbt[7], g_objectRbt[8]);
  Bezier bc3(g_objectRbt[ 8], g_objectRbt[ 9], g_objectRbt[10], g_objectRbt[11]);
  Bezier bc4(g_objectRbt[11], g_objectRbt[12], g_objectRbt[13], g_objectRbt[14]);
  Bezier bc5(g_objectRbt[14], g_objectRbt[15], g_objectRbt[16], g_objectRbt[2]);

  Matrix4 bezMove;
  Matrix4 bezFwrd;

  if(g_animClock < 1){
    bezMove = bc1.getBez(g_animClock);
    bezFwrd = bc1.getBez(g_animClock+.001);
  }
  if(g_animClock > 1 && g_animClock < 2){
    bezMove = bc2.getBez(g_animClock - 1);
    bezFwrd = bc2.getBez(g_animClock - .999);
  }
  if(g_animClock > 2 && g_animClock < 3){
    bezMove = bc3.getBez(g_animClock - 2);
    bezFwrd = bc3.getBez(g_animClock - 1.999);
  }
  if(g_animClock > 3 && g_animClock < 4){
    bezMove = bc4.getBez(g_animClock - 3);
    bezFwrd = bc4.getBez(g_animClock - 2.999);
  }
  if(g_animClock > 4 && g_animClock < 5){
    bezMove = bc5.getBez(g_animClock - 4);
    bezFwrd = bc5.getBez(g_animClock - 3.999);
  }

  Cvec3 z = normalize(Cvec3(bezMove(0,3), bezMove(1,3), bezMove(2,3)) - Cvec3(bezFwrd(0,3), bezFwrd(1,3), bezFwrd(2,3)));
  Cvec3 zRight =  normalize(Cvec3(bezMove(1,3), bezMove(2,3), bezMove(1,3)) - Cvec3(bezFwrd(1,3), bezFwrd(2,3), bezFwrd(0,3)));
  Cvec3 x = normalize(cross(Cvec3(0,1,0), z));
  Cvec3 y = cross(z, x);

  g_objectRbt[0] = bezMove;

  for(int i=0; i<3; i++){
    for(int j=0; j<3; j++){
      if(i==0)
	g_objectRbt[0](j,i) = x[j];
      if(i==1)
	g_objectRbt[0](j,i) = y[j];
      if(i==2)
	g_objectRbt[0](j,i) = z[j];
    }
  }

  if(view == 1)
    g_eyeRbt = g_objectRbt[0];
  else
    g_eyeRbt = Matrix4::makeTranslation(Cvec3(0.0, 3.25, 10.0));
  const Matrix4 invEyeRbt = inv(g_eyeRbt); // store inverse so we don't have to recompute it
  const Cvec3 eyeLight1 = Cvec3(invEyeRbt * Cvec4(g_light1, 1)); // g_light1 position in eye coordinates
  const Cvec3 eyeLight2 = Cvec3(invEyeRbt * Cvec4(g_light2, 1)); // g_light2 position in eye coordinates

  glUseProgram(curSS.program); // select shader we want to use
  sendProjectionMatrix(curSS, projmat); // send projection matrix to shader
  safe_glUniform3f(curSS.h_uLight, eyeLight1[0], eyeLight1[1], eyeLight1[2]); // shaders need light positions
  safe_glUniform3f(curSS.h_uLight2, eyeLight2[0], eyeLight2[1], eyeLight2[2]);

  Matrix4 MVM;
  Matrix4 NMVM;

  // Start Stick figure Geometry
  //Body
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(0,-.75,0));
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1); 
  if(view == 1)
    glUseProgram(solidSS.program);					   
  g_tube->draw(curSS);
  glUseProgram(curSS.program);

  //Head
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(0,-.05,-1));
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0, 1, 0); 
  if(view == 1)
    glUseProgram(solidSS.program);					   
  g_sphere->draw(curSS);
  glUseProgram(curSS.program);

  //Eyes
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(.3,-.05,-1.95)) * Matrix4::makeScale(.2);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0, 0, 0); 
  if(view == 1)
    glUseProgram(solidSS.program);					   
  g_sphere->draw(curSS);
  glUseProgram(curSS.program);

  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(-.3,-.05,-1.95)) * Matrix4::makeScale(.2);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0, 0, 0); 
  if(view == 1)
    glUseProgram(solidSS.program);					   
  g_sphere->draw(curSS);
  glUseProgram(curSS.program);
  

  //Right Arm
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(.5,-.75,-1)) * Matrix4::makeYRotation(-30);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1); 					   
  g_tube->draw(curSS);
  glUseProgram(curSS.program);

  //Left Arm
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(-.5,-.75,-1)) * Matrix4::makeYRotation(30);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1); 					   
  g_tube->draw(curSS);
  glUseProgram(curSS.program);

  //Right Leg
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(.3,-.75,1.8)) * Matrix4::makeYRotation(20);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  g_tube->draw(curSS);
  glUseProgram(curSS.program);

  //Left Arm
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(-.5,-.75,-1)) * Matrix4::makeYRotation(30);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1); 					   
  g_tube->draw(curSS);
  glUseProgram(curSS.program);

  //Right Leg
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(.3,-.75,1.8)) * Matrix4::makeYRotation(20);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1); 
  if(view == 1)
    glUseProgram(solidSS.program);					   
  g_tube->draw(curSS);
  glUseProgram(curSS.program);

  //Left Leg
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(-.3,-.75,1.8)) * Matrix4::makeYRotation(-20);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1); 
  if(view == 1)
    glUseProgram(solidSS.program);					   
  g_tube->draw(curSS);
  glUseProgram(curSS.program);

  //Hands
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(1.0125,-.75,-1.9)) * Matrix4::makeScale(.3);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1); 					   
  g_sphere->draw(curSS);
  glUseProgram(curSS.program);

  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(-1.0125,-.75,-1.9)) * Matrix4::makeScale(.3);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1); 					   
  g_sphere->draw(curSS);
  glUseProgram(curSS.program);

  //Legs
  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(.625,-.75,2.65)) * Matrix4::makeScale(.3);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1);
  if(view == 1)
    glUseProgram(solidSS.program);				   
  g_sphere->draw(curSS);
  glUseProgram(curSS.program);

  MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(-.625,-.75,2.65)) * Matrix4::makeScale(.3);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1); 
  if(view == 1)
    glUseProgram(solidSS.program);					   
  g_sphere->draw(curSS);
  glUseProgram(curSS.program);
  //End Stick figure Geometry

  //Scenery
  glUseProgram(curSS.program);
  for(int x=-10; x<11; x+=5){
    for(int y=-5; y<16; y+=5){
      for(int z=-5; z<6; z+=5){
	MVM = invEyeRbt * g_objectRbt[1] 
	                * Matrix4::makeScale(abs(z) * .125 + abs(x) * .125 + abs(y) * .125 + .125) 
	                * Matrix4::makeTranslation(Cvec3(x,y+1,z)) 
	                * Matrix4::makeYRotation(g_animClock*90)
	                * Matrix4::makeXRotation(g_animClock*90);
	NMVM = normalMatrix(MVM);
	sendModelViewNormalMatrix(curSS, MVM, NMVM);
	if(g_animClock < 2.5)
	  safe_glUniform3f(curSS.h_uColor, 0, g_animClock/5, g_animClock/5); //abs(x%4), abs(y%4), abs(z%4));
	else
	  safe_glUniform3f(curSS.h_uColor, 0, .5*abs((g_animClock-5)/2.5), .5*abs((g_animClock-5)/2.5)); //abs(x%4), abs(y%4), abs(z%4));
	g_octahedron->draw(curSS);
      }
    }
  }

  //Control Points
  for(int i=2; i<g_numObjects-1; i++){
    MVM = invEyeRbt * g_objectRbt[i] * Matrix4::makeScale(.25);
    NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    if(g_objToManip == i)
      safe_glUniform3f(curSS.h_uColor, 1, 1, 0);
    else
      safe_glUniform3f(curSS.h_uColor, 0, 0, 1);
    g_sphere->draw(curSS);
  }

  //Bezier Path
  if(viewPath){
    if(view == 0){
      for(int w=1; w<6; w++){
	for(int q=0; q<100; q++){
	  if(w == 1)
	    path = bc1.getBez(q*.01);
	  if(w == 2)
	    path = bc2.getBez(q*.01);
	  if(w == 3)
	    path = bc3.getBez(q*.01);
	  if(w == 4)
	    path = bc4.getBez(q*.01);
	  if(w == 5)
	    path = bc5.getBez(q*.01);	    
	  MVM = invEyeRbt * path * Matrix4::makeScale(.1);
	  NMVM = normalMatrix(MVM);
	  sendModelViewNormalMatrix(curSS, MVM, NMVM);
	  safe_glUniform3f(curSS.h_uColor, 1, 0, 0);
	  g_sphere->draw(curSS);
	}
      }
    }
    else {
      for(int w=1; w<6; w++){
	for(int q=0; q<10; q++){
	  if(w == 1)
	    path = bc1.getBez(q*.1);
	  if(w == 2)
	    path = bc2.getBez(q*.1);
	  if(w == 3)
	    path = bc3.getBez(q*.1);
	  if(w == 4)
	    path = bc4.getBez(q*.1);
	  if(w == 5)
	    path = bc5.getBez(q*.1);	    
	  MVM = invEyeRbt * path * Matrix4::makeScale(.035);
	  NMVM = normalMatrix(MVM);
	  sendModelViewNormalMatrix(curSS, MVM, NMVM);
	  safe_glUniform3f(curSS.h_uColor, 1, 0, 0);
	  g_sphere->draw(curSS);
	}
      }
    }
  }
}

static void display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);   // clear framebuffer color&depth
  drawScene();
  glutSwapBuffers();                                    // show the back buffer (where we rendered stuff)
  checkGlErrors();

  // Calculate frames per second 
  static int oldTime = -1;
  static int frames = 0;
  static int lastTime = -1;

  int currentTime = glutGet(GLUT_ELAPSED_TIME); // returns milliseconds
  g_elapsedTime = currentTime - lastTime;       // how long last frame took
  lastTime = currentTime;

        if (oldTime < 0)
                oldTime = currentTime;

        frames++;

        if (currentTime - oldTime >= 5000) // report FPS every 5 seconds
        {
                cout << "Frames per second: "
                        << float(frames)*1000.0/(currentTime - oldTime) << endl;
                cout << "Elapsed ms since last frame: " << g_elapsedTime << endl;
                oldTime = currentTime;
                frames = 0;
        }
}

static void reshape(const int w, const int h) {
  g_windowWidth = w;
  g_windowHeight = h;
  glViewport(0, 0, w, h);
  updateFrustFovY();
  glutPostRedisplay();
}

static void motion(const int x, const int y) {
  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  Matrix4 m, a;
  if (g_mouseLClickButton && !g_mouseRClickButton) { // left button down?
    m = Matrix4::makeTranslation(Cvec3(dx, dy, 0) * 0.01);
  }
  else if (g_mouseRClickButton && !g_mouseLClickButton) { // right button down?
    m = Matrix4::makeTranslation(Cvec3(0, 0, -dy) * 0.01);
  }

  if (g_mouseClickDown) {
    a = transFact(g_objectRbt[g_objToManip])*linFact(g_eyeRbt);
    if(g_objToManip == 2){
      g_objectRbt[g_objToManip] = a * m * inv(a) * g_objectRbt[g_objToManip];
      g_objectRbt[g_objToManip + 15] = a * m * inv(a) * g_objectRbt[g_objToManip];
      /*
      // to keep c1 continuity
      a = transFact(g_objectRbt[g_objToManip + 14])*linFact(g_eyeRbt);
      g_objectRbt[g_objToManip + 14] = a * m * inv(a) * g_objectRbt[g_objToManip + 14];
      a = transFact(g_objectRbt[g_objToManip + 1])*linFact(g_eyeRbt);
      g_objectRbt[g_objToManip + 1] = a * m * inv(a) * g_objectRbt[g_objToManip + 1];
      */
    }
    else{
      g_objectRbt[g_objToManip] = a * m * inv(a) * g_objectRbt[g_objToManip];
      /*
      // to keep c1 continuity
      a = transFact(g_objectRbt[g_objToManip + 1])*linFact(g_eyeRbt);
      g_objectRbt[g_objToManip + 1] = a * m * inv(a) * g_objectRbt[g_objToManip + 1];
      a = transFact(g_objectRbt[g_objToManip - 1])*linFact(g_eyeRbt);
      g_objectRbt[g_objToManip - 1] = a * m * inv(a) * g_objectRbt[g_objToManip - 1];
      */
    }
    glutPostRedisplay(); // we always redraw if we changed the scene
  }

  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;
}

static void mouse(const int button, const int state, const int x, const int y) {
  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system

  g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
  g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
  g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

  g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
  g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
  g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

  g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;
}

static void idle()
{
  g_animIncrement = g_animSpeed * g_elapsedTime / 1000; // rescale animation increment
  g_animClock += g_animIncrement;           // Update animation clock 
         if (g_animClock > g_animMax)       // and cycle to start if necessary.
		 g_animClock = g_animStart;
         glutPostRedisplay();  // for animation
}

static void keyboard(const unsigned char key, const int x, const int y) {
  switch (key) {
  case 27:
    exit(0);                                  // ESC
  case 'h':
    cout << " ============== H E L P ==============\n\n"
    << "h\t\thelp menu\n"
    << "s\t\tsave screenshot\n"
    << "o\t\tCycle object to manipulate\n"
    << "f\t\tCycle fragment shader\n"
    << "v\t\tCycle view\n"
    << "p\t\tPath view \n"
    << "+\t\tIncrease animation speed\n"
    << "-\t\tDecrease animation speed\n"
    << "drag left mouse to rotate\n" 
    << "drag middle mouse to translate in/out \n" 
    << "drag right mouse to translate up/down/left/right\n" 
    << endl;
    break;
  case 's':
    glFlush();
    writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
    cout << "Screenshot written to out.ppm." << endl;
    break;
  case 'o':
    g_objToManip = (g_objToManip +1) % g_numObjects;
    if(g_objToManip == 0)
      g_objToManip = 2;
    break;
  case 'v':
    view = (view +1) % 2;
    break;
  case 'p':
    if(viewPath)
      viewPath = false;
    else 
      viewPath = true;
    break;
  case '+':
    g_animSpeed *= 1.05;
    break;
  case '-':
    g_animSpeed *= 0.95;
    break;
  }
  glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
  glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
  glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
  glutCreateWindow("Project 4: Equilibrium");    // title the window

  glutDisplayFunc(display);                               // display rendering callback
  glutReshapeFunc(reshape);                               // window reshape callback
  glutMotionFunc(motion);                                 // mouse movement callback
  glutMouseFunc(mouse);                                   // mouse click callback
  glutIdleFunc(idle);  					  // idle callback for animation
  glutKeyboardFunc(keyboard);
}

static void initGLState() {
  glClearColor(0, 0, 0, 0.);
  glClearDepth(0.);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glCullFace(GL_BACK);
//  glEnable(GL_CULL_FACE); // Enable if you don't want to render back faces,
                            // but make sure it's disabled to show inside of tube.
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_GREATER);
  glReadBuffer(GL_BACK);
  glEnable(GL_BLEND); // Enable alpha blending
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  if (!g_Gl2Compatible)
    glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initShaders() {
  g_shaderStates.resize(g_numShaders);
  for (int i = 0; i < g_numShaders; ++i) {
    if (g_Gl2Compatible)
      g_shaderStates[i].reset(new ShaderState(g_shaderFilesGl2[i][0], g_shaderFilesGl2[i][1]));
    else
      g_shaderStates[i].reset(new ShaderState(g_shaderFiles[i][0], g_shaderFiles[i][1]));
  }
}

static void initGeometry() {
  initObjects();
}

int main(int argc, char * argv[]) {
  try {
    initGlutState(argc,argv);

    glewInit(); // load the OpenGL extensions

    cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.3") << endl;
    if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
    else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");

    initGLState();
    initShaders();
    initGeometry();
    
    glutMainLoop();

    return 0;
  }
  catch (const runtime_error& e) {
    cout << "Exception caught: " << e.what() << endl;
    return -1;
  }
}
